public interface Plot {

    public int getAbcisse() ;
    public int getOrdonne();

    //
    public Plot getPlotTo(Direction direction, int i );

    //public int get_Plot_To(Direction direction);
}

