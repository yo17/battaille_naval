public class BoardPlot implements Plot {
    /* ajouter implements pour dire de quoi herite la classe dans se cas cest Plot */
    private int abcisse;
    private int  ordonne;


    /* genere un constructeur afin d'implementer les coordonne */
    public BoardPlot(int abcisse, int ordonne) {
        this.abcisse = abcisse;
        this.ordonne = ordonne;
    }

    public int getAbcisse() {
        return abcisse;
    }

    public int getOrdonne() {
        return ordonne;
    }


/* genere notre methode   qui sera overrid  qui vas recalculer nos coordonne en fonction de la direction */
    @Override
    public Plot getPlotTo(Direction direction, int i ) {
        // car il sait que Plot a 2 argumment abscisse et ordonne

        switch(direction) {
            case  NORTH :
               ordonne = ordonne + i;
            case SOUTH:
                ordonne = ordonne - i;
            case EAST:
                abcisse = abcisse + i;
            default:
                abcisse = abcisse - i;
        }
        // attention on appel boardplot et pas plot car c'est une interface
        return  new BoardPlot(this.abcisse,this.ordonne);
    }
}
